#!/bin/sh

set -e

#
# Set default values for parameters
#
INIT=0

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    --init)
        INIT=1
        ;;
    *)
        ;;
esac
shift # past argument or value
done

#
# Run script
#
if [ ${INIT} -eq 1 ]; then
    if [ ! -f /var/www/dynamic_cfg/initialised ]; then
        sh -x /usr/local/bin/alpine-httpd__init_entrypoint.sh

        if [ ! -z ${TRUST_SELF_SIGNED_CERTIFICATE+x} ]; then
            if [ ${TRUST_SELF_SIGNED_CERTIFICATE} -eq 1 ]; then
                echo "SSLProxyVerify none" >> /var/www/dynamic_cfg/website_extra.conf
                echo "SSLProxyCheckPeerName off" >> /var/www/dynamic_cfg/website_extra.conf
                echo "SSLProxyCheckPeerCN off" >> /var/www/dynamic_cfg/website_extra.conf
                #Explicitly trust the self signed certificates of backends
                #SSLProxyCACertificateFile TLS_key_store/bundle.pem
            fi
        fi


        chown -R apache:www-data /var/www/TLS_key_store/

        httpd -e DEBUG -t

        #finish initialisation
        touch /var/www/dynamic_cfg/initialised
    else
        echo "Already initialised"
    fi
else
    while  [ ! -f  '/var/www/dynamic_cfg/initialised' ]; do sleep 10; done
    dumb-init --single-child httpd -D FOREGROUND
fi


